import {useState, useCallback} from 'react';


export const  UseFetch = ( ) => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
        const request = useCallback(
            async (url, method = 'GET', body = null, headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            } ) => {
            setLoading(true);
            try {
                const responce = await fetch(url, method, body, headers = {});

                const data =  responce.json();
                if (!responce.ok) {
                    setError(data.message);
                    throw new Error(data.message || "Error conection to data")
                }
                setLoading(false);
                return data;
            } catch (e) {
                setLoading(false);
                setError(e.message);
                throw e;
            }
        }, []);
    const clearError = () => setError(null);

    return {loading, request, error, clearError};

}

