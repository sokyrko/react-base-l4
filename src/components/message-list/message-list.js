import React, {useState, useEffect} from 'react';
/* eslint-disable */
import Modal from '../common/modal/modal';
import Segment from '../common/segment/segment';
import TextArea from '../common/text-area/text-area';
import Message from '../Message/message';
import OwnMessage from '../own-message/own-message';
import {v4 as uuid} from "uuid";
import {datetimeFormat} from '../common/helper/data.helper';
import style from './styles.module.scss';
const MessageList = ({dataMessages, userId,  onUpdate, onDelete, onLiked}) => {
    const res = dataMessages;
    // const isEdit = false;
    const [isOpen, setIsOpen] = useState(false);
    const [editElm, setEditElm] = useState(null)
    const [avatar, setAvatar]=useState(null);
    const [text, setText]=useState(null);
    const [temp, setTemp]=useState();
    const [tempAvatar, setTempAvatar]=useState('');

    const handleOnClose = () => {
        setIsOpen(false);
        onMessageUpdate();
    };

    const handleIsEdit = (elmEdit)=>{
        setIsOpen(true);
        setEditElm(elmEdit);
    }
    const handleDelete = (id) =>{
        onDelete(id);
    }
    const handleLiked = (id) => {
        onLiked(id)
    }
    const onMessageUpdate = () =>{
        const user = "Random Name";
        const createdAt = datetimeFormat();
        const editedAt = datetimeFormat();
        return onUpdate({id: editElm.id, userId, avatar: editElm.avatar, user, text:editElm.text, createdAt: editElm.createAt, editedAt});
    }
    const onSaveText =  (e) => {
        e.preventDefault();
        const text = e.target.value;
        setEditElm({...editElm, text:text});
    } 

    const onSaveFile = (e) => {
        const file = e.target.files[0];
        setTemp(file)
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            setAvatar(reader.result);
            setEditElm({...editElm, avatar: reader.result});
        }
    }
    useEffect(
        () => {
            const userAvatar = (() => res.find(elm => elm.userId === userId))();
            const tempUserAvatar = (typeof userAvatar !== "undefined") ? userAvatar.avatar : 'https://bootdey.com/img/Content/avatar/avatar7.png';
            setTempAvatar(tempUserAvatar);
        }, [])
    return (
        <div className={style.messageList}>

            <div className = "chat-history" >
                <ul className = "m-b-0" >

            {res.map(elm => {
                return  elm.userId !==userId ?
                     (<Message key={Math.random(10000)*1000} id={elm.id} avatar = {elm.avatar}
                               text = {elm.text} createdAt={elm.createdAt} onLiked={handleLiked}/>)
                     :  (<OwnMessage key={Math.random(10000)*1000}   id={elm.id} avatar = {elm.avatar===null? tempAvatar:elm.avatar}
                                     text = {elm.text}   isEdit={handleIsEdit} onDelete={handleDelete}/>)
                }

            )}
                </ul>
            </div>

            {/*{isOpen} */}
                <Modal
                    isOpen = {isOpen}
                    onClose = {handleOnClose}
                    isCentered
                >
                    <Segment >
                        <form onSubmit = {onMessageUpdate} >
                            <TextArea
                                name="areaTarget"
                                refs="areaTarget"
                                value = {editElm !== null ? editElm.text : ""}
                                onChange = {onSaveText}
                                placeholder = ""
                            />
                            <img src = {editElm !== null ? editElm.avatar : ""} alt = "avatar" />
                            <input id="change_avatar" type = "file" placeholder = "Type text ..." onChange = {onSaveFile} />
                            <input type = "button" value = "Submit" onClick = {handleOnClose} />
                        </form >
                    </Segment >
                </Modal >
            {/*:*/}
        </div >
    );
};

export default MessageList;