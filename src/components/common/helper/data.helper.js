export const datetimeFormat = () => {
    const date = new Date();
    let dateTime = date.getFullYear() + "-" + date.getMonth() + 1
        + "-" + date.getDate()
        + "T" + date.getHours() + ":"
        + date.getMinutes() + ":"
        + date.getSeconds() + ".936Z";
    // console.log(dateTime);
    return dateTime;
}