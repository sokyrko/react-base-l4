import React from 'react';
import style from './styles.module.scss';
import Spinner from '../common/spinner/spinner';
const Preloader = () => {
    return (
        // loading ===true ?
        <div className={style.preloader}>
            <Spinner isOverflow={true}/>
        </div >
        // :''
    );

};

export default Preloader;