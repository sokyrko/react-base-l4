import React from 'react';
import PropTypes from 'prop-types';

const Header =({messagesCount, participapntsCount, lastTime}) => {

    return (
        <div >
            <span>{participapntsCount} participiants</span>
            <span>{messagesCount} messages</span>
            <span>Last message at {lastTime} </span>
        </div >
    );
};

Header.propTypes = {
    countParticipants: PropTypes.string,
    countMessages : PropTypes.string,
    lastDate: PropTypes.string
};

export default Header;