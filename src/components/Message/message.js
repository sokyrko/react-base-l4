import React , {useState} from 'react';
import   './styles.module.scss';
const Message = ({id, avatar, text, createdAt, onLiked}) => {
    // const handleIsEdit = () =>{
    //     const message = {avatar: avatar, text: text};
    //     isEdit(message);
    // }
    const [liked, setLiked] = useState(false);
const handlePostLike = ()=>{
    setLiked(!liked)
    onLiked({id, liked})
}
    return (

        <li className = "clearfix" >
            <div className = "message-data" >
                <span className = "message-data-time" >{createdAt}</span >
                <img src= {avatar} alt="avatar"/>
            </div >

            <div className = "message my-message" >{text}</div >
            { liked?
                <i className = "fa fa-thumbs-up liked" onClick = {handlePostLike} />
                :
                <i className = "fa fa-thumbs-down liked" onClick = {handlePostLike} />
            }
        </li >

    );
};

export default Message;