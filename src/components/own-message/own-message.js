import React from 'react';
import {datetimeFormat} from '../common/helper/data.helper';

const OwnMessage = ({id, avatar, text, createdAt,  isEdit, onDelete}) => {
    const curId = id;
    const handleIsEdit = () =>{
        const timeUp = datetimeFormat();
        const message = {id, avatar, text: text, createdAt: timeUp};
        isEdit(message);
    }
    const handleIsDelete = () =>{
        onDelete(curId)
    }
    return (
        <li className = "clearfix" >
            <div className = "message-data text-right" >
                <span className = "message-data-time" >{createdAt} </span >
                <img className="message-data-time float-right" src = {avatar} alt = "avatar" />
            </div >
            <div className = "message other-message float-right" >
                <br/>
                <br/>
                {text}
                <br/>
                <br/>
                <input  className="message-data-time float-right" type = "button" value="Edit" onClick={handleIsEdit}/>
                <input className="message-data-time float-right" type = "button" value="Delete" onClick={handleIsDelete}/>

            </div >
        </li >


    );
};

export default OwnMessage;