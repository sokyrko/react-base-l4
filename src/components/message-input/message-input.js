import React , {useState} from 'react';
import { v4 as uuid } from 'uuid';
import {datetimeFormat} from '../common/helper/data.helper';
import './styles.module.scss'

const MessageInput = ({onAddMessage}) => {
    const [avatar, setAvatar]=useState(null);
    const [text, setText]=useState(null);

    const onSaveText = (e) => {
        const text = e.target.value;
        setText(text);
    }
    const onSaveFile = (e) => {
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.onload = () => setAvatar(reader.result)
        reader.readAsDataURL(file)
    }
const onSubmit = () =>{
    const id = uuid();
    const user = "Random Name";
    const createdAt = datetimeFormat();
    const editedAt = datetimeFormat();
    // console.log(createdAt);
    return onAddMessage({id,  avatar, user, text , createdAt, editedAt});
}
    return (


        <div   className = "chat-message fixed-bottom text-justify" >
            <div id="input-group" className = "input-group1 mr-5 pr-x-5 " >
                <div className = "input-group-prepend" >
                    <span className = "input-group-text" ><i className = "fa fa-send" ></i ></span >
                </div >
                <input type = "text" placeholder="Type text ..." onChange={onSaveText}/>
                <input type = "file" placeholder="Change avatar ..." onChange={onSaveFile} />

                <input  type="button" onClick={onSubmit} value="submit"/>
            </div >
        </div >


    );
};

export default MessageInput;