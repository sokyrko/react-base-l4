import React, {useState, useEffect} from 'react';
import { UseFetch} from '../hooks/fetch.hooks';
import PropTypes from 'prop-types';

import Header from './Header/header';
import MessageList from './message-list/message-list';
import Preloader from './preloader/preloader';
import MessageInput from './message-input/message-input';
import './styles.module.scss'

const Chat = ({url, userId}) => {
    const storage = window.localStorage;
    const urlJson = url;
    const curUserId = userId.userId;
    const {request} = UseFetch();
    const [data, setData] = useState([]);
    const liked = [];
    const [messageCount, setMessageCount] = useState('');
    const [participapntsCount, setParticipapntsCount] = useState('');
    const [participapnts, setParticipapnts] = useState([]);
    const [lastTime, setLastTime] = useState();
    const [loading, setLoading] = useState(true);
    const [curUserAvatar, setCurUserAvatar] = useState("");


    const handleAddMessage =  messageData =>{
        const message = {userId:curUserId, ...messageData};
        setData([...data, message]);

    };
    const handleUpdate =  messageData =>{
        Object.keys(data).forEach(key=>{
            if(data[key].id===messageData.id) {
                data[key] = {...messageData}
            }
        })
    };
    const handleDelete =  id => {
        const dataF = data.filter(el=> el.id !== id);
        setData( dataF);
    }
    const handleLiked = data => {
        if(!liked.length) liked.push(data)
        //can to do
        // Object.keys(liked).forEach(key=>{
        //     if(liked[key].id===data.id) {
        //         data[key] = data
        //     }
        // })
        storage.setItem('liked',JSON.stringify( liked));
        const test =  JSON.parse(localStorage.getItem("liked"));
        Object.keys(test).forEach(key=>{
        })
            // console.log(storage.getItem('liked'));
    }
    useEffect(  ()=>{
        const getData= async () => {
            request(urlJson, {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }).then((data) =>{
                setData(data);
                setMessageCount(data.length);
                const userAvatar = (() => data.find(elm => elm.userId === curUserId))();
                const tempUserAvatar =  (typeof userAvatar !== "undefined") ? userAvatar.avatar : 'https://bootdey.com/img/Content/avatar/avatar7.png';
                setCurUserAvatar(tempUserAvatar);
                let res = [];
                if (data.length) data.forEach(elm => {
                    if ( !res.find( el => el.userId === elm.userId )) {
                        res.push(elm);
                    }
                });
                setParticipapntsCount(res.length);
                setParticipapnts(res);

                const {createdAt} = data.slice(-1)[0];
                setLastTime(createdAt.substr(11, 5))
                setInterval(()=>
                    setLoading(false),500
                )
            })
        }
        getData();

    },[urlJson,request,curUserId])


    return  loading ?(<Preloader loading={loading}/>): (
        <div className = "card chat-app" >
            <div id = "plist" className = "people-list" >
                <div className = "input-group" >
                    <div className = "input-group-prepend" >
                        <span className = "input-group-text" ><i className = "fa fa-search" ></i ></span >
                    </div >
                    <input type = "text" className = "form-control" placeholder = "Can to do but i didn't have much time Sorry!!" />
                </div >
                <ul className = "list-unstyled chat-list mt-2 mb-0" >
                    {participapnts.map(elm => {
                        return (<li  key={elm.id} className = "clearfix" >
                            <img src = {elm.avatar} alt = "avatar" />
                            <div className = "about" >
                                <div className = "name" >{elm.user}</div >
                                <div className = "status" >
                                    <i className = "fa fa-circle offline" ></i >
                                    left 7 mins ago
                                </div >
                            </div >
                        </li >)
                    })}
                </ul>
            </div>

            <div className = "chat" >
                <div className = "chat-header clearfix" >
                    <h1 >Chat</h1 >


                    <Header messagesCount = {messageCount} participapntsCount = {participapntsCount}
                            lastTime = {lastTime} />
                    <MessageList dataMessages = {data} userId = {curUserId} onUpdate = {handleUpdate}
                                 onDelete = {handleDelete} curUserAvatar={curUserAvatar} onLiked={handleLiked}/>
                    <MessageInput onAddMessage = {handleAddMessage} curUserAvatar={curUserAvatar}/>

                </div >
            </div >
        </div>
    );
};
Header.propTypes = {
    url: PropTypes.string,
    userId : PropTypes.string,
};
export default Chat;