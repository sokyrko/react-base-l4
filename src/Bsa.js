import Chat from './components/chat';
import './Bsa.css';
import {v4 as uuid} from "uuid";

const Bsa = () => {
    const userId = {userId :uuid()};

    return (
        <div className = "container" >
            <div className = "row clearfix" >
                <div className = "col-lg-12" >
                    {/*<div className = "card chat-app" >*/}

                        <Chat url = "messages.json" userId = {userId} />
                    {/*</div >*/}
                </div >
            </div >
        </div >
    );
}

export default Bsa;
